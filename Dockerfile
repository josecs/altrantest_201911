#
# The script has been developed and tested with Python 3.6 using the 'requests' module
#
FROM python:3.6-slim

RUN pip install requests

ADD HoeWarmIsHetInDelft.py /

CMD [ "python", "/HoeWarmIsHetInDelft.py" ]
