#!/usr/bin/python3

# After an analysis of the WeerInDelft home page contents, I realized that all the source data is periodically
# collected from a URL that renders a text file with a format called ClientRAW used to transfer meteorological data.
# The current outside temperature, measured in Celsius degrees, is at the 5th position.
# You can find further information about this format at:
#
# http://www.meteomincio.it/wxclientraw.php
# http://curtiszmweather.com/wxclientrawparser.php
#
# I decided to use this file to gather the current temperature in Delft to avoid the use of heavier tools or
# libraries like Selenium or BeautifulSoup.

import time
import requests

clientRawHeader = '12345'

msjError_OpeningURL = 'HWIHID_ERROR: Exception trying to open URL [{}]'
msjError_GettingValues = 'HWIHID_ERROR: Could not get source data'
msjOutput_Temperature = '{} degrees Celsius'

mainURL = 'http://www.weerindelft.nl'
myURLFormat = mainURL + '/clientraw.txt?{}'

# It is not necessary to set the timezone as Epoch time is TZ-independent
myURL = myURLFormat.format(int(time.time()))

try:
    httpResponse = requests.get(myURL)
except Exception as myXcept:
    print(msjError_OpeningURL.format(myXcept))
    exit(1)
else:
    pageText = httpResponse.text.split()
    if pageText[0] == clientRawHeader:
        print(msjOutput_Temperature.format(round(float(pageText[4]))))
    else:
        print(msjError_GettingValues)
        exit(2)
    httpResponse.close()
